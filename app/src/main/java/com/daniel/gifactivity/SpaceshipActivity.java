package com.daniel.gifactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SpaceshipActivity extends AppCompatActivity {
    private ImageView imageView;
    private AnimationDrawable animation = new AnimationDrawable();
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spaceship);
        getWindow().setStatusBarColor(getResources().getColor(R.color.spaceship));
        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
        imageView.setImageDrawable(animation);

        String sImage;
        for (int i = 1; i <= 225; i++) {
            sImage = "spaceship";
            animation.addFrame(
                    ResourcesCompat.getDrawable(
                            getResources(), getResources().getIdentifier(
                                    sImage + i, "drawable", getPackageName()),
                            null),
                    20);
        }
        animation.setOneShot(false);


        animation.start();
        textView.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), BaymaxActivity.class);
            startActivity(intent);
        });

    }
}
