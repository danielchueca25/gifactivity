package com.daniel.gifactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class BaymaxActivity extends AppCompatActivity {
    private ImageView imageView;
    private AnimationDrawable animation;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.baymax_activity);
        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
        //Baymax
        imageView.setBackgroundResource(R.drawable.animation);
        animation = (AnimationDrawable) imageView.getBackground();
        getWindow().setStatusBarColor(getResources().getColor(R.color.Pink));

        textView.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), RocketActivity.class);
            startActivity(intent);
        });


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        animation.start();
    }


}